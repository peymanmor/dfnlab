# __DFN.Lab__

Welcome to the Gitlab page dedicated to DFN.lab.

## Description

DFN.Lab is a software suite for Discrete Fracture Network analyses. It includes various modules for generation, analyses, flow, transport and mechanics.
See https://www.fractorylab.com for more details.

## Contents

This repository contains tutorials on how to use DFN.Lab, and some advanced application examples. The master branch contains the tutorials for the latest numbered version. If you got an older version, go to the TAG of the version you're using now or ask for an update.

##### Tutorials
0. Python and DFN.Lab
1. Create a domain
2. Create tunnels and wells
3. Create deterministic fractures and DFNs
4. Input and output DFN file format in DFN.Lab
5. Stochastic DFN generation
6. UFM-DFN generation
7. Hydraulic boundary conditions
8. Meshing DFNs
9. Solving flow (steady state)

Please keep updated, we are adding new tutorials on a regular basis, covering other applications like site models, inert transport simulations, heat transfert simulations or mechanical properties of fractures rocks.

## The Team

DFN.lab is developed by the *Fractory*, a joint laboratory between Itasca Consultants, the CNRS and the University of Rennes.

## Where is the code

The code is not freely available, please contact r.legoc@itasca.fr or philippe.davy@univ-rennes1.fr to ask for an access. We provide  free access only to collaborators in academic research.

You will find a this page some tutorials on the use of DFN.lab that you can run once you get the code. You can also submit tickets if you have any question/bugs.
