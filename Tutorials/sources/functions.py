def isOnTestServer() :
    import os.path
    from os import path
    return path.exists('D:/Shares/Users/Fractory/GitLab-Runner/python37/python.exe')

# def plot_dfn(fnet,filename,value=None,log=False,interactive=False):
#     if isOnTestServer() : return
#     import dfnlab.IO as dfn_io
#     import pyvista as pv
#     import numpy as np

#     dfn_io.write_DFN_file(fnet,filename)
#     visu = pv.read(filename+".vtp")
#     p = pv.Plotter(notebook=True)
#     p.set_background('white')
#     argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
#     if value==None:
#         p.add_mesh(visu, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
#     else:
#         if log==False:
#             p.add_mesh(visu, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
#         else:
#             visu['log('+value+')'] = np.log(visu[value])
#             p.add_mesh(visu, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)

#     p.show(auto_close=False)

def plot_dfn_from_vtp_file(filename,value=None,log=False,interactive=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np
    visu = pv.read(filename+".vtp")
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    if value==None:
        p.add_mesh(visu, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
    else:
        if log==False:
            p.add_mesh(visu, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
        else:
            visu['log('+value+')'] = np.log(visu[value])
            p.add_mesh(visu, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
    p.show(auto_close=False)

def plot_dfn(fnet,filename,value=None,log=False,interactive=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np

    io_obj = dfn_io.DFNIO_Object(fnet)
    if value!=None:
        io_obj.appendDataOnDFN(value)
    io_obj.createVTKDataSystem()
    io_obj.writeVTKOnDFN(filename)
    io_obj.writeVTKOnSystem(filename+'_system')
    
    visu = pv.read(filename+".vtp")
    visu2 = pv.read(filename+"_system.vtp")
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    if value==None:
        p.add_mesh(visu, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
    else:
        if log==False:
            p.add_mesh(visu, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
        else:
            visu['log('+value+')'] = np.log(visu[value])
            p.add_mesh(visu, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
            
    p.add_mesh(visu2, style='wireframe', color='black')

    p.show(auto_close=False)
    
    

# def plot_dfn_and_system(fnet,filename,value=None,log=False,interactive=False):
#     if isOnTestServer() : return
#     import dfnlab.IO as dfn_io
#     import pyvista as pv
#     import numpy as np

#     dfn_io.write_DFN_file(fnet,filename+'_dfn')
    
#     io_obj2 = dfn_io.DFNIO_Object(fnet)
#     io_obj2.createVTKDataSystem()
#     io_obj2.writeVTKOnSystem(filename+'_system')
    
#     p = pv.Plotter(notebook=True)
#     p.set_background('white')
#     argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
#     visu1 = pv.read(filename+"_dfn.vtp")
#     visu2 = pv.read(filename+"_system.vtp")
    
#     if value==None:
#         p.add_mesh(visu1, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
#     else:
#         if log==False:
#             p.add_mesh(visu1, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
#         else:
#             visu['log('+value+')'] = np.log(visu[value])
#             p.add_mesh(visu1, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
#     p.add_mesh(visu2, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)

#     p.show(auto_close=False)
    
  
    

def plot_stereonet(fnet):
    if isOnTestServer() : return
    import mplstereonet
    import pylab as pl
    import matplotlib.pyplot as pyplot
    pyplot.rcParams['figure.dpi'] = 150

    strike = []
    dip =[]
    for frac in fnet:
        strike.append(frac.dipD()[1]-90.)
        dip.append(frac.dipD()[0])
    fig = pyplot.figure()
    ax1 = fig.add_subplot(121, projection='stereonet')
    pos = ax1.density_contourf(strike, dip, measurement='poles', method='schmidt',gridsize=50,extend='neither',cmap='coolwarm')
    ax1.set_title('Contour density')
    ax1.set_azimuth_ticks([])
    fig.colorbar(pos, ax=ax1)
    ax1.grid(kind='polar')
    ax2 = fig.add_subplot(122, projection='stereonet')
    ax2.pole(strike, dip, markersize=0.1)
    ax2.set_title('Fracture poles')
    ax2.grid(kind='polar')
    pyplot.show()


def plot_size_distribution(fnet):
    if isOnTestServer() : return
    import matplotlib.pyplot as pyplot
    pyplot.rcParams['figure.dpi'] = 100

    csfont = {'fontname':'Times New Roman', 'fontsize':14}
    pyplot.rcParams['mathtext.fontset'] = 'dejavuserif'
    fig = pyplot.figure()
    ax = pyplot.axes()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel('Fracture size $l$',**csfont)
    ax.set_ylabel('Fracture size distribution $n(l)$',**csfont)
    size_distribution = fnet.sizeDistribution(nb_bin=20)
    lmean = size_distribution["Val_min"]
    nl =size_distribution['Pdf']
    ax.plot(lmean,nl,marker='o',markersize=5,linestyle='-',label = 'UFM',color='blue')
    pyplot.title("Fracture size distribution")
    pyplot.show()


def plot_correlation_pair_function(fnet,D,rmin=1,nbin=20):
    if isOnTestServer() : return
    import matplotlib.pyplot as pyplot
    import dfnlab.Analysis as dfnA
    import numpy as np

    analyser = dfnA.DFNAnalyser(fnet)
    c2r = analyser.correlationPairFunction3D(rmin,nbin)

    # prepare plot
    csfont = {'fontname':'Times New Roman', 'fontsize':14}
    pyplot.rcParams['mathtext.fontset'] = 'dejavuserif'
    pyplot.rcParams['figure.dpi'] = 100
    fig = pyplot.figure() #(figsize=(8, 7))
    ax = pyplot.axes()
    ax.set_xscale("log")
    ax.set_yscale("log")

    # get
    r = np.array(c2r['r_mean'])
    idx1 = [True if r[i]!=0 else False for i in range(0,len(r)) ]
    Dc = np.array(c2r['correlation_dimension'])
    idx2 = [True if Dc[i]!=0 else False for i in range(0,len(Dc)) ]
    idx = idx1 and idx2
    r = r[idx]
    Dc = Dc[idx]
    ax.plot(r,Dc,marker='o',markersize=5, linewidth=2, color='k',label='DFN')
    ax.set_ylim(2,3.5)
    ax.axhline(D,linestyle = 'dashed', color = 'grey', label='$D='+str(D)+'$')

    pyplot.xlabel('Pair distance $r$',**csfont)
    pyplot.ylabel('Correlation dimension $D$',**csfont)
    pyplot.title("Correlation dimension")
    pyplot.legend()
    pyplot.show()

def plot_mesh(mesh,filename,field=None, fieldName = None):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np
    Aw = dfn_io.DFNIO_Object(mesh)
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)

    if(not(field  == None or fieldName == None)):
        Aw.appendDataOnMesh(field,fieldName)

    Aw.writeVTKOnMesh(filename,True)
    Aw.createVTKDataSystem()
    Aw.writeVTKOnSystem(filename+'_system')

    visu = pv.read(filename+".vtp")
    visu2 = pv.read(filename+"_system.vtp")
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    if(field  == None or fieldName == None):
        p.add_mesh(visu, show_edges=True, color='white')
    else:
        p.add_mesh(visu, scalars=fieldName, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
        
    p.add_mesh(visu2, style='wireframe', color='black')

    p.show(auto_close=False)


def plot_system(system,filename,value=None,interactive=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import dfnlab.Basis as basis
    import pyvista as pv
    import numpy as np

    fnet = basis.DFN(system)
    io_obj2 = dfn_io.DFNIO_Object(fnet)
    io_obj2.createVTKDataSystem()
#     if value!=None:
#         io_obj2.appendDataOnSystem(value)
    io_obj2.writeVTKOnSystem(filename)
    nbGeometry = system.nbWellTunnel()
    if nbGeometry>0:
        io_obj2.createVTKDataGeometries()
        io_obj2.writeVTKOnGeometries(filename+'_geometries')
    
    visu = pv.read(filename+'.vtp')
    if nbGeometry>0:
        visu2 = pv.read(filename+'_geometries.vtp')
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    if value==None:
        p.add_mesh(visu, style='wireframe', color='black')
    else:
        p.add_mesh(visu, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
    if nbGeometry>0:
        p.add_mesh(visu2, color='grey', show_edges=False, lighting=True)
    p.show(auto_close=False)

def plot_tunnel(pathFolder,fnet) :
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    #Create a writer from our DFN
    writer = dfn_io.DFNIO_Object(fnet)
    #Write a VTP for the DFN
    writer.createVTKDataDFN()
    #Write a VTP for the intersection (we only have intersection with the geometry as we didn't compute the others)
    writer.createVTKDataIntersections()
    #Write a VTP for the Tunnel
    writer.createVTKDataGeometries()

    writer.writeVTKOnIntersections(pathFolder+"/intersections");
    writer.writeVTKOnGeometries(pathFolder+"/Tunnel");
    writer.writeVTKOnDFN(pathFolder+"/dfn");

    import pyvista as pv
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    visu_dfn = pv.read(pathFolder+"/dfn.vtp")
    p.add_mesh(visu_dfn, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True, opacity=0.05)
    visu_tunnel = pv.read(pathFolder+"/Tunnel.vtp")
    p.add_mesh(visu_tunnel, color='grey', show_edges=False, edge_color='black', lighting=True, show_scalar_bar=True, opacity=0.9)
    visu_inter = pv.read(pathFolder+"/intersections.vtp")
    p.add_mesh(visu_inter, color='red', show_edges=False, edge_color='black', lighting=True, show_scalar_bar=True)
    p.show(auto_close=False)

def plot_octree(pathFolder,system) :
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import dfnlab.Basis as dfn_basis
    import pyvista as pv
    
    #Create a dfn from our system
    fnet = dfn_basis.DFN(system)
    #Create a writer from our DFN
    writer = dfn_io.DFNIO_Object(fnet)
    
    #Write a VTU for the Octree
    writer.createVTKDataOctree();
    writer.writeVTKOnOctree(pathFolder+"/Octree")

    #Configure The Plot
    p = pv.Plotter(shape=(1,2), notebook=True, window_size=[1500,500])
    p.set_background('white')
    mesh_octree = pv.read(pathFolder+"/Octree.vtu")
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    mesh_domains = pv.read(pathFolder+"/Domains.vtp")

    p.subplot(0, 0)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 1, show_scalar_bar= False)
    p.add_text("Octree Visualisation", color='black')
    
    p.subplot(0, 1)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 0.02, show_scalar_bar= False)
    p.add_mesh(mesh_octree.slice(normal=[1, 0, 0],origin=[20,0,0]), scalars='domain density', edge_color='white', cmap='coolwarm',lighting=True, show_scalar_bar= False)
    p.add_text("Slice octree Visualisation", color='black')

    p.show(auto_close=False)

def plot_octreeAndDomains(pathFolder,system) :
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import dfnlab.Basis as dfn_basis
    import pyvista as pv
    
    #Create a dfn from our system
    fnet = dfn_basis.DFN(system)
    #Create a writer from our DFN
    writer = dfn_io.DFNIO_Object(fnet)
    
    #Write a VTP for the Domains
    writer.createVTKDataFractureDomains()
    writer.writeVTKOnFractureDomains(pathFolder+"/Domains")
    
    #Write a VTU for the Octree
    writer.createVTKDataOctree();
    writer.writeVTKOnOctree(pathFolder+"/Octree")

    #Configure The Plot
    p = pv.Plotter(shape=(1,3), notebook=True, window_size=[1500,500])
    p.set_background('white')
    mesh_octree = pv.read(pathFolder+"/Octree.vtu")
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    mesh_domains = pv.read(pathFolder+"/Domains.vtp")
  
    p.subplot(0, 0)
    p.add_mesh(mesh_domains, color='white', show_edges=False, edge_color='black', lighting=True, opacity=0.3)   
    p.add_text("Domains Visualisation", color='black')

    p.subplot(0, 1)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 1, show_scalar_bar= False)
    p.add_text("Octree Visualisation", color='black')
    
    p.subplot(0, 2)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 0.02, show_scalar_bar= False)
    p.add_mesh(mesh_domains.slice(normal=[1, 0, 0],origin=[20,0,0]),line_width =2, color='black', show_edges=True, edge_color='black', lighting=True, opacity=0.3, show_scalar_bar= False)   
    p.add_mesh(mesh_octree.slice(normal=[1, 0, 0],origin=[20,0,0]), scalars='domain density', edge_color='white', cmap='coolwarm',lighting=True, show_scalar_bar= False)
    p.add_text("Slice octree Visualisation", color='black')

    p.show(auto_close=False)
    
    
def plot_dfnWithOctreeAndDomains(pathFolder,dfn) :
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import dfnlab.Basis as dfn_basis
    import pyvista as pv

    #Create a writer from our DFN
    writer = dfn_io.DFNIO_Object(dfn)
    
    #Write a VTP for the Domains
    writer.createVTKDataFractureDomains()
    writer.writeVTKOnFractureDomains(pathFolder+"/Domains")
    
    #Write a VTU for the Octree
    writer.createVTKDataOctree();
    writer.writeVTKOnOctree(pathFolder+"/Octree")
    
    #Write a VTU for the DFN
    writer.createVTKDataDFN();
    writer.writeVTKOnDFN(pathFolder+"/DFN")

    #Configure The Plot
    p = pv.Plotter(shape=(2,3), notebook=True, window_size=[1500,1000])
    p.set_background('white')
    mesh_octree = pv.read(pathFolder+"/Octree.vtu")
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    mesh_domains = pv.read(pathFolder+"/Domains.vtp")
    dfnVisu = pv.read(pathFolder+"/DFN.vtp")
  
    p.subplot(0, 0)
    p.add_mesh(mesh_domains, color='white', show_edges=False, edge_color='black', lighting=True, opacity=0.3)   
    p.add_text("Domains Visualisation", color='black')

    p.subplot(0, 1)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 1, show_scalar_bar= False)
    p.add_text("Octree Visualisation", color='black')
    
    p.subplot(0, 2)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 0.02, show_scalar_bar= False)
    p.add_mesh(mesh_domains.slice(normal=[1, 0, 0],origin=[20,0,0]),line_width =2, color='black', show_edges=True, edge_color='black', lighting=True, opacity=0.3, show_scalar_bar= False)   
    p.add_mesh(mesh_octree.slice(normal=[1, 0, 0],origin=[20,0,0]), scalars='domain density', edge_color='white', cmap='coolwarm',lighting=True, show_scalar_bar= False)
    p.add_text("Slice octree Visualisation", color='black')
        
    p.subplot(1, 0)
    p.add_mesh(dfnVisu, color='white',cmap='coolwarm', scalars='FractureSize', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=False)
    p.add_text("DFN_UFM Visualisation", color='black')
 
    p.subplot(1, 1)
    p.add_mesh(dfnVisu, color='white',cmap='coolwarm', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=False)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=True, show_scalar_bar= False,opacity=0.75)
    p.add_text("DFN_UFM in Octree Visualisation", color='black')

    p.subplot(1, 2)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 0.02,show_scalar_bar= False)
    p.add_mesh(mesh_domains.slice(normal=[1, 0, 0],origin=[20,0,0]),line_width =2, color='black', show_edges=True, edge_color='black', lighting=False, opacity=0.3, show_scalar_bar= False)   
    p.add_mesh(mesh_octree.slice(normal=[1, 0, 0],origin=[20,0,0]), scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, show_scalar_bar= False)
    p.add_mesh(dfnVisu.slice(normal=[1, 0, 0],origin=[20,0,0]),line_width =3, color='black', show_edges=True, edge_color='black', lighting=False)   
    p.add_text("Slice dfn Visualisation", color='black')

    p.show(auto_close=False)
    

def plot_dfnWithOctree(pathFolder,dfn) :
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import dfnlab.Basis as dfn_basis
    import pyvista as pv

    #Create a writer from our DFN
    writer = dfn_io.DFNIO_Object(dfn)
    
    #Write a VTU for the Octree
    writer.createVTKDataOctree();
    writer.writeVTKOnOctree(pathFolder+"/Octree")
    
    #Write a VTU for the DFN
    writer.createVTKDataDFN();
    writer.writeVTKOnDFN(pathFolder+"/DFN")

    #Configure The Plot
    p = pv.Plotter(shape=(1,3), notebook=True, window_size=[1500,500])
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    p.set_background('white')
    mesh_octree = pv.read(pathFolder+"/Octree.vtu")
    
    dfnVisu = pv.read(pathFolder+"/DFN.vtp")
       
    p.subplot(0, 0)
    p.add_mesh(dfnVisu, color='white',cmap='coolwarm', scalars='FractureSize', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=False)
    p.add_text("DFN_UFM Visualisation", color='black')
 
    p.subplot(0, 1)
    p.add_mesh(dfnVisu, color='white',cmap='coolwarm', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=False)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=True, show_scalar_bar= False,opacity=0.75)
    p.add_text("DFN_UFM in Octree Visualisation", color='black')

    p.subplot(0, 2)
    p.add_mesh(mesh_octree, scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, opacity = 0.02,show_scalar_bar= False)
    p.add_mesh(mesh_octree.slice(normal=[1, 0, 0],origin=[20,0,0]), scalars='domain density', edge_color='white', cmap='coolwarm',lighting=False, show_scalar_bar= False)
    p.add_mesh(dfnVisu.slice(normal=[1, 0, 0],origin=[20,0,0]),line_width =3, color='black', show_edges=True, edge_color='black', lighting=False)   
    p.add_text("Slice dfn Visualisation", color='black')

    p.show(auto_close=False)

    
def plot_GraphsVsDFN(dfn_results, fgraph_results, igraph_results, name):
    if isOnTestServer() : return
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('DFN '+str(name), fontsize = 15)
    ax.set_ylabel('Graph '+str(name), fontsize = 15)
    ax.tick_params(axis="x", labelsize=15)
    ax.tick_params(axis="y", labelsize=15)
    ax.plot(dfn_results, fgraph_results, 'o', markersize = 1, color = 'blue', label = 'fracture graph')
    ax.plot(dfn_results, igraph_results, 'o', markersize = 1, color = 'red', label = 'intersection graph')
    ax.legend()
    ax.set_yscale('log')
    ax.set_xscale('log')
    #line y=x
    max_value = max(max(dfn_results),max(fgraph_results),max(igraph_results))
    ax.plot([0,max_value],[0,max_value], color = 'black', linewidth = 1)

    plt.show()


    
def plot_flowGraph(filename) :
    if isOnTestServer() : return
    import pyvista as pv

    p = pv.Plotter(notebook=True)
    p.set_background('white')
    visu_fgraph = pv.PolyData(filename)
    edge_scalars = visu_fgraph.cell_arrays['edgeFlow']
    p.add_mesh(visu_fgraph, scalars=edge_scalars, line_width=2, show_scalar_bar=False,  log_scale=True)
    p.add_scalar_bar(color = 'black', title = 'Edge flow Q', label_font_size=12,title_font_size=16, position_x = 0.8, position_y =1 )
    p.show_grid(color='black')
    
    p.show(auto_close=False)
    
    
def plot_FlowParticles(mesh, particles, filenameFlow, fileNameParticles,field=None, fieldName = None):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np
    Aw = dfn_io.DFNIO_Object(mesh)
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)

    if(not(field  == None or fieldName == None)):
        Aw.appendDataOnMesh(field,fieldName)

    Aw.writeVTKOnMesh(filenameFlow,True)
    dfn_io.write_particle_file( particles,fileNameParticles+".vtp")

    visu = pv.read(filenameFlow+".vtp")
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    if(field  == None or fieldName == None):
        p.add_mesh(visu, show_edges=True, color='white')
    else:
        p.add_mesh(visu, scalars=fieldName, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)

    visu = pv.read(fileNameParticles+".vtp")
 
    p.add_mesh(visu, color = 'black',line_width = 2)      
    p.show(auto_close=False)



def plot_dfn_structures(fullDFN, connectedDFN, backboneDFN, pathFolder, interactive=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np


    io_obj = dfn_io.DFNIO_Object(fullDFN)
    io_obj.createVTKDataSystem()
    io_obj.writeVTKOnDFN(pathFolder+'/dfn')
    io_obj.writeVTKOnSystem(pathFolder+'/dfn_system')
    dfn_io.write_DFN_file(connectedDFN, pathFolder+'/connectedDFN')
    dfn_io.write_DFN_file(backboneDFN, pathFolder+'/backboneDFN')

    visu1 = pv.read(pathFolder+"/dfn.vtp")
    visu2 = pv.read(pathFolder+"/connectedDFN.vtp")
    visu3 = pv.read(pathFolder+"/backboneDFN.vtp")
    visu4 = pv.read(pathFolder+"/dfn_system.vtp")

    p = pv.Plotter(notebook=True)
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)

    p.add_mesh(visu1, color='green', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True, label='full DFN')
    p.add_mesh(visu2, color='blue', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True, label='connected DFN')
    p.add_mesh(visu3, color='red', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True, label='backbone')
    p.add_mesh(visu4, style='wireframe', color='black')

    p.add_legend(bcolor = None)
    p.set_background('white')

    p.show(auto_close=False)


def plot_dfn_and_wells(fnet,filename,value=None,log=False,interactive=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np

    io_obj = dfn_io.DFNIO_Object(fnet)
    if value!=None:
        io_obj.appendDataOnDFN(value)
    io_obj.createVTKDataSystem()
    io_obj.createVTKDataGeometries()
    io_obj.writeVTKOnDFN(filename)
    io_obj.writeVTKOnSystem(filename+'_system')
    io_obj.writeVTKOnGeometries(filename+"_wells");
    
    visu = pv.read(filename+".vtp")
    visu2 = pv.read(filename+"_system.vtp")
    visu3 = pv.read(filename+"_wells.vtp")
    p = pv.Plotter(notebook=True)
    p.set_background('white')
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    if value==None:
        p.add_mesh(visu, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True, opacity = 0.6)
    else:
        if log==False:
            p.add_mesh(visu, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss, opacity = 0.6)
        else:
            visu['log('+value+')'] = np.log(visu[value])
            p.add_mesh(visu, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss, opacity = 0.6)
            
    p.add_mesh(visu2, style='wireframe', color='black')
    p.add_mesh(visu3, style='wireframe', line_width=8, color='black')

    p.show(auto_close=False)
    
    